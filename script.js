function getAndShow(array,parent = document.body) {
    let fragment = document.createDocumentFragment();
    let ul = document.createElement("ul");
    fragment.appendChild(ul);
    let element;
    array.forEach(p => {
        if (Array.isArray(p)) {
            element = document.createElement("ul");
            ul.appendChild(element);
            getAndShow(p,element)
        } else {
            element = document.createElement("li");
            element.textContent = p;
            ul.appendChild(element);
        }
    });
    parent.appendChild(fragment);
}
const array = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
getAndShow(array);

setTimeout(document.body.innerHTML = "",3000);